import { ref } from 'vue';
import type { Ref } from 'vue';
import { useStudentStore } from '@/stores/student';
import type IStudent from '@/interface/IStudent';

const url = 'https://65e8dab54bb72f0a9c508303.mockapi.io/dev/api/Alumnos';

export class AuthService {
    private students: Ref<IStudent[]> = ref([]);
    private student: Ref<IStudent> = ref({} as IStudent);
    private token: Ref<string> = ref('');
    private url: string = url;

    constructor() {}

    getStudents(): Ref<IStudent[]> {
        return this.students;
    }

    getStudent(): Ref<IStudent> {
        return this.student;
    }

    async fetchStudents(): Promise<void> {
        try {
            const response = await fetch(this.url + '/', {
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                }
            });
            const data = await response.json();
            this.students.value = data;
        } catch (error) {
            console.error(error);
        }
    }

    async fetchStudent(id: string): Promise<void> {
        try {
            const response = await fetch(this.url + '/' + id);
            const data: IStudent = await response.json();
            this.student.value = data;
        } catch (error) {
            console.error('Error fetching student data:', error);
        }  
    }

    async createStudent(data: any) {
        try {
            await fetch(this.url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });
        } catch (error) {
            console.error('Error al crear el alumno:' + error);
        }
    }

    async deleteStudent(id: number) {
        try {
            await fetch(`${this.url}/${id}`, {
                method: 'DELETE'
            });
        } catch (error) {
            console.error('Error al eliminar el alumno:', error);
        }
    }

    async login(email: string, password: string): Promise<boolean> {
        if (!email || !password) {
            return false;
        }

        const fromData = { email, password };

        try {
            const response = await fetch(this.url + '/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(fromData)
            });

            const jsonResponse = await response.json();

            if (response.ok) {
                this.token = jsonResponse.token;
                return this.token ? true : false;
            } else {
                console.error('Error al iniciar sesión: ', jsonResponse.message);
                return false;
            }
        } catch (error) {
            console.error('Error de red', error);
            return false;
        }
    }
}
