import { ref } from 'vue'
import { AuthService } from '@/service/AuthService'
import type IStudent  from '@/interface/IStudent'
import { defineStore } from 'pinia'
import type { Ref } from 'vue'

export const useStudentStore = defineStore('students', () => {
  const students = ref([]) as Ref<IStudent[]>
  const student = ref({} as IStudent)

  const authService = new AuthService()

  async function fetchStudents() {
    try {
      await authService.fetchStudents()
      students.value = authService.getStudents().value
    } catch (error) {
      console.error(error)
    }
  }

  async function fetchStudent(id: string) {
    try {
      await authService.fetchStudent(id)
      student.value = authService.getStudent().value
    } catch (error) {
      console.error(error)
    }
  }

  async function createStudent(data: any) {
    try {
      await authService.createStudent(data)
      await fetchStudents()
    } catch (error) {
      console.error(error)
    }
  }

  async function deleteStudent(id: number) {
    try {
      await authService.deleteStudent(id)
      await fetchStudents()
    } catch (error) {
      console.error(error)
    }
  }

  async function login(email: string, password: string): Promise<boolean> {
    try {
      return await authService.login(email, password)
    } catch (error) {
      console.error(error)
      return false
    }
  }

  return {
    students,
    student,
    fetchStudents,
    fetchStudent,
    createStudent,
    deleteStudent,
    login,
  }
})
